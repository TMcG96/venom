from flask import Flask, request, jsonify
from flask_cors import CORS
import mysql.connector
import json

app = Flask(__name__)
cors = CORS(app)

# searchpages
@app.route('/')
def main():

    connection = mysql.connector.connect(
        host='10.102.192.3',
        user='root',
        passwd='tommyuzz',
        database='Websites'
    )

    connectionAdds = mysql.connector.connect(
            host='10.102.192.3',
            user='root',
            passwd='tommyuzz',
            database='Adds'
    )

    search = request.args.get('q')
    query = "SELECT uri FROM page WHERE content LIKE '%" + search + "%'"
    cursor = connection.cursor()
    cursor.execute(query)
    records = cursor.fetchall()

    queryAdd = "SELECT advert,Link FROM advert WHERE keyword LIKE '%" + search + "%'"
    cursor = connectionAdds.cursor()
    cursor.execute(queryAdd)
    recordsAdds = cursor.fetchall()
    if(len(recordsAdds) == 0):
        queryAdd = "SELECT advert,Link FROM advert WHERE keyword LIKE '%Default%'"
        cursor = connectionAdds.cursor()
        cursor.execute(queryAdd)
        recordsAdds = cursor.fetchall()

    response = jsonify({
    'records': records,
    'adds': recordsAdds
    })

    return response

@app.route('/test')
def test():
    return('hello!')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port = '5001')
